package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class ResultActivity: AppCompatActivity() {
    var mGrade: TextView ?= null
    var mFinalScore: TextView ?= null
    var mRetryButton: Button ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result);
        mGrade = findViewById(R.id.grade);
        mFinalScore = findViewById(R.id.outOf);
        mRetryButton = findViewById(R.id.retry);

        val bundle = intent.extras
        val score = bundle!!.getInt("finalScore")

        mFinalScore!!.setText("You scored " + score + " out of " + QuizBook.questions.length)

        if (score == 5) {
            mGrade!!.setText("Outstanding")
        } else if (score == 4) {
            mGrade!!.setText("Good Work")
        } else if (score == 3) {
            mGrade!!.setText("Good Effort")
        } else {
            mGrade!!.setText("Go over your notes")
        }

        mRetryButton!!.setOnClickListener {
            var intent = Intent(this, QuizActivity::class.java)
            startActivity(intent)
            this.finish()
        }

    }

}
