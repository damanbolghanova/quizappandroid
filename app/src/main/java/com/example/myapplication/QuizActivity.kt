package com.example.myapplication

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class QuizActivity: AppCompatActivity() {

    var mScoreView: TextView? = null
    var mQuestion: TextView? = null
    var mImageView: ImageView? = null
    var mTrueButton: Button? = null
    var mFalseButton: Button? = null

    private var mAnswer = false
    private var mScore = 0
    private var mQuestionNumber = 0

    var res: Resources = resources
    var questions = res.getStringArray(R.array.questions)

    var images = {
        R.drawable.washington
        R.drawable.sun
        R.drawable.earth
        R.drawable.bulb
    }


    var answers = {
        true
        false
        false
        true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        mScoreView = findViewById(R.id.points)
        mImageView = findViewById(R.id.imageView);
        mQuestion = findViewById(R.id.question);
        mTrueButton = findViewById(R.id.trueButton);
        mFalseButton = findViewById(R.id.falseButton);

        updateQuestion()



        mTrueButton!!.setOnClickListener {
            if (mAnswer == true){
                mScore++
                updateScore(mScore)

                if (mQuestionNumber == questions.size) {
                    val i = Intent(this, ResultActivity::class.java)
                    val bundle = Bundle()
                    bundle.putInt("finalScore", mScore)
                    i.putExtras(bundle)
                    this.finish()
                    startActivity(i)
                } else {
                    updateQuestion()
                }
            }
            else{
                if (mQuestionNumber === questions.size) {
                    val i = Intent(this, ResultActivity::class.java)
                    val bundle = Bundle()
                    bundle.putInt("finalScore", mScore)
                    i.putExtras(bundle)
                    this.finish()
                    startActivity(i)
                } else {
                    updateQuestion()
                }
            }
        }

        mFalseButton!!.setOnClickListener {
            if (mAnswer == false) {
                mScore++
                updateScore(mScore)
                if (mQuestionNumber == questions.size) {
                    val i = Intent(this, ResultActivity::class.java)
                    val bundle = Bundle()
                    bundle.putInt("finalScore", mScore)
                    i.putExtras(bundle)
                    this@QuizActivity.finish()
                    startActivity(i)
                } else {
                    updateQuestion()
                }
            } else {
                if (mQuestionNumber == questions.size) {
                    val i = Intent(this, ResultActivity::class.java)
                    val bundle = Bundle()
                    bundle.putInt("finalScore", mScore)
                    i.putExtras(bundle)
                    this.finish()
                    startActivity(i)
                } else {
                    updateQuestion()
                }
            }
        }
    }

    private fun updateQuestion(){
        val images1 = images
        mImageView!!.setImageResource(images[mQuestionNumber])
        mQuestion.text(questions[mQuestionNumber])
        mAnswer = answers[mQuestionNumber];
        mQuestionNumber++;
    }

    private fun updateScore(point: Int){
        mScoreView!!.text("" + mScore)
    }

    private fun clickExit(){
        askToClose()
    }

    override fun onBackPressed() {
        askToClose()
    }

    private fun askToClose(){
        var builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to quit?")
        builder.setCancelable(true)
        builder.setPositiveButton("OK"
        ) { dialog, which -> finish() }

        builder.setNegativeButton("Cancel"
        )  { dialog, which -> dialog.cancel() }

        var alert: AlertDialog = builder.create()
        alert.show()
    }


}