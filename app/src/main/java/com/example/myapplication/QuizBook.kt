package com.example.myapplication

import android.R
import android.util.Log.w


class QuizBook {
    var questions = arrayOf(
        "Washington, D.C. is the capital of the United States of America.",
        "The sun revolves around the Earth.",
        "Christopher Columbus proved that the Earth was round.",
        "Queen Elizabeth II and Marilyn Monroe were born the same year.",
        "Thomas Edison invented the light bulb."
    )

    var images = intArrayOf(
        R.drawable., R.drawable.sun, R.drawable.photo2, R.drawable.queen, R.drawable.lamp
    )
    var answers = booleanArrayOf(
        true, false, false, true, false
    )
}